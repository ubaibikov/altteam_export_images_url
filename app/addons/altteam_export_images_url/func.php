<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_exim_get_all_images_url($product_id, $position ,$lang_code)
{
    if($position == 1){
        $image_pairs = fn_get_image_pairs($product_id, 'product', 'M', false, true, $lang_code);    
    }else{
        $image_pairs = fn_get_image_pairs($product_id, 'product', 'A', false, true, $lang_code);
        $position = $position - 2; // if positon in additional image start with 0
    }
   
    $protocol = fn_get_storefront_protocol();

    if(isset($image_pairs['position']) && $image_pairs['position'] == 0){
        return $image_pairs['detailed'][$protocol . '_image_path'] ?? '';
    }

    foreach($image_pairs as $_image_pair)
    {
        $image_paths[] = $_image_pair['detailed'][$protocol . '_image_path'] ?? '';
    }

    if(!empty($image_paths)){
        return $image_paths[$position];
    }
    
    return '';
}