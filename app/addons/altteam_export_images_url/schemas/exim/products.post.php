<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/

use Tygh\Registry;

$qty_image_url = trim(Registry::get('addons.altteam_export_images_url.quantity_image_export'));
$image_url = !empty($qty_image_url) ? $qty_image_url : 2;  

for ($i = 1;$i <= $image_url ;$i++){
    $schema['export_fields']["Url image $i"] = array(
        'process_get' => array('fn_exim_get_all_images_url', '#key', $i, '#lang_code'),
        'db_field' => 'image_id',
        'table' => 'images_links',
        'multilang' => true,
        'export_only' => true,
    );
}

return $schema;
